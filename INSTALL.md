Installing LTLf2FOL
==================

    cd LTLf2FOL/ltlf2fol
    make
    make run
    
Installing LTL2HOA
==================
LTL2HOA depends on LTL2FOL and software MONA. 
MONA is included as a submodule. 
To download the submodule andinstall mona, run:

    git submodule update --init --recursive
    cd ext/MONA/
    autoreconf -f -i
    ./configure --prefix=`pwd`
    make
    make install-strip
    
LTL2FOL and MONA must be installed locally in the predefined folder.
Otherwise, the path to the binaries must be updated in ltl2hoa.py.