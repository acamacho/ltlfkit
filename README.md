LTLfKit Version 0.1. A toolkit for Finite Linear Temporal Logic.
Copyright (C) 2017 Alberto Camacho

This toolkit contains several tools for managing Linear Temporal Logic formulas
interpreted over finite traces (LTLf).

Please read the file 'COPYING' before you use LTLfKit.
To compile and install, see the file 'INSTALL'.
The different dependencies are property of their authors
and released under their own copyright terms and conditions.

If you have any problems or questions about the source code,
please send an email to

  acamacho@cs.toronto.edu


CREDIT
======
This is the LTLf to DFA translator that I used in SynKit and LTLFOND2FOND. This tool is a wrapper that makes use of Zhu et al. (2017)'s LTLf to DFA translator via transformations to First Order Logic, which uses MONA.
The main advantage of using this tool is that the produced DFAs are output in standard HOA format, which can be input to Spot for handling automata.

If you think the use of this tool deserves a citation in your paper (thanks!), then I would suggest you cite our SynKit paper at ICAPS-18, and Zhu et al.'s IJCAI-17 paper.



        @inproceedings{cam-bai-mui-mci-icaps18,
          author    = {Alberto Camacho and
                       Jorge A. Baier and
                       Christian J. Muise and
                       Sheila A. McIlraith},
          title     = {Finite {LTL} Synthesis as Planning},
          booktitle = {Proceedings of the Twenty-Eight International Conference on Automated Planning and Scheduling ({ICAPS})},
          pages     = {29--38},
          year      = {2018}
        }


        @inproceedings{DBLP:conf/ijcai/ZhuTLPV17,
          author    = {Shufang Zhu and
                       Lucas M. Tabajara and
                       Jianwen Li and
                       Geguang Pu and
                       Moshe Y. Vardi},
          title     = {Symbolic LTLf Synthesis},
          booktitle = {Proceedings of the Twenty-Sixth International Joint Conference on Artificial Intelligence ({IJCAI})},
          pages     = {1362--1369},
          year      = {2017}
        }


USAGE
=====
To transform an LTLf formula into First Order Logic (FOL):

    ./LTLf2FOL/ltl2fol/ltl2fol formula.ltl
    
To transform an LTLf formula into a deterministic finite-state automaton (DFA):

    python3 ltlf2hoa.py <formula>
    
To transform an LTLf formula into NNF and break it into smaller subformulae
linked with a logical connective:

    python3 ltlf2nnf.py <formula>