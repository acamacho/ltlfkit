import os
dirname = os.path.dirname(os.path.abspath(__file__))

LTL2NNF = os.path.join(os.path.join(dirname,os.path.join('LTLf2FOL','ltlf2fol')),'ltlf2nnf')
    
def normalize_connective(connective):
    if "&&" in connective:
        return "and"
    elif "||" in connective:
        return "or"
    else:
        return ""

def break_nnf(fml):
    # breaks NNF into two smaller NNF formulae
    ltl_filename = "formula.ltl"
    f = open(ltl_filename,"w")
    f.write(fml)
    f.close()
    
    import subprocess
    cmd = "%s %s break" %  (LTL2NNF,ltl_filename)
    # print(cmd)
    p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    ltlf2nnf_output, err = p.communicate()
    ltlf2nnf_output = str(ltlf2nnf_output, 'utf-8').strip()
    print(ltlf2nnf_output)
    connective,left,right = ltlf2nnf_output.split('\t')
    
    return normalize_connective(connective),left,right
    
def ltl2nnf(fml):
    ltl_filename = "formula.ltl"
    f = open(ltl_filename,"w")
    f.write(fml)
    f.close()
    
    import subprocess
    cmd = "%s %s convert" %  (LTL2NNF,ltl_filename)
    # print(cmd)
    p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    ltlf2nnf_output, err = p.communicate()
    nnf = str(ltlf2nnf_output, 'utf-8').strip()
    
    return nnf
    
if __name__ == "__main__":
    import sys
    args = sys.argv[1:]
    
    fml = args[0]
    nnf = ltl2nnf(fml)
    print("NNF: %s" % nnf)
    connective,left,right = break_nnf(nnf)
    print("Connective: %s" % connective)
    print("Left: %s" % left)
    print("Right: %s" % right)

