import os,tempfile
dirname = os.path.dirname(os.path.abspath(__file__))

LTL2MONA = os.path.join(os.path.join(dirname,os.path.join('LTLf2FOL','ltlf2fol')),'ltlf2fol')
MONA = os.path.join(os.path.join(dirname,os.path.join(os.path.join('ext','MONA'),'bin')),'mona -w -q')

UNSAT_HOA = """HOA: v1
States: 1
Start: 0
AP: 0
acc-name: Finite-State
Acceptance: 1 Inf(0)
--BODY--
State: 0
[t] 0
--END--"""

# modified from krrt.utils
# bitbucket.org/haz/krtoolkit
def get_value(text, regex, value_type = float):
    """ Lifts a value from a file based on a regex passed in. """
    import re
    # Get the text of the time
    pattern = re.compile(regex, re.MULTILINE)
    results = pattern.search(text)
    if results:
        return value_type(results.group(1))
    else:
        print("Could not find the value, %s, in the text provided" % (regex))
        return(value_type(0.0))
        
def LTLf2Mona(fml,mona_filename):
    # Use the TemporaryFile "formula.ltl"
    tmp = tempfile.NamedTemporaryFile(delete=True)
    f = open(tmp.name, 'w')
    f.write(fml)
    f.close()
        
    import subprocess
    cmd = "%s %s BNF" %  (LTL2MONA,tmp.name)
    # print(cmd)
    p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    ltlf2mona_output, err = p.communicate()
    ltlf2mona_output = str(ltlf2mona_output, 'utf-8').strip()
    f = open(mona_filename,"w")
    f.write(ltlf2mona_output)
    # f.close()


def parse_mona(mona_output):
    free_variables = get_value(mona_output, '.*DFA for formula with free variables:[\s]*(.*?)\n.*', str)
    free_variables = [x.strip().lower() for x in free_variables.split() if len(x.strip()) > 0]
    assert("alive" in free_variables)
    alive_var_idx = free_variables.index("alive")
    # substract the "alive" variable
    num_AP = len(free_variables) - 1

    initial_state = get_value(mona_output, '.*Initial state:[\s]*(\d+)\n.*', int)

    accepting_states = get_value(mona_output, '.*Accepting states:[\s]*(.*?)\n.*', str)
    accepting_states = [int(x.strip()) for x in accepting_states.split() if len(x.strip()) > 0]
    
    num_states = get_value(mona_output, '.*Automaton has[\s]*(\d+)[\s]states.*', int)

    
    cur_state = -1
    hoa = "HOA: v1\n"
    hoa += "States: %s\n" % num_states
    # hoa += "Start: %s\n" % initial_state
    hoa += "Start: 1\n"
    hoa += 'AP: %s %s\n' % (num_AP, ' '.join('"%s"' % literal for literal in free_variables if literal != "alive" ))
    hoa += "acc-name: Finite-State\n"
    hoa += "Acceptance: 1 Inf(0)\n"
    # hoa += "# properties: trans-labels explicit-labels complete stutter-invariant\n"
    hoa += "--BODY--"
    
    # the dictionary maps a dest_id automaton state 
    # to a list of guards, where each guard is a set
    # guard_dict = {}
    for line in mona_output.splitlines():
        if line.startswith("State "):
            orig_state = get_value(line, '.*State[\s]*(\d+):\s.*', int)
            guard = get_value(line, '.*:[\s](.*?)[\s]->.*', str)
            dest_state = get_value(line, '.*state[\s]*(\d+)[\s]*.*', int)
            # print(dest_state)
            if orig_state != cur_state:
                hoa += "\nState: %s" % orig_state
                cur_state = orig_state
                # # clear dictionary to start monitoring a new orig state
                # guard_dict.clear()

            # if dest_state not in guard_dict:
            #     guard_dict[dest_state] = []
            hoa_guard =[]
            alive_guard = True
            assert(len(guard) == len(free_variables))
            for m in range(len(guard)):
                if m == alive_var_idx:
                    # assert(guard[i] in ["X","1"])
                    if guard[m] in ["X","1"]:
                        continue
                    else:
                        assert(guard[m] == "0")
                        alive_guard = False
                        break
                i = m
                if m > alive_var_idx:
                    i -= 1
                    
                if guard[m] == "X":
                    continue
                elif guard[m] == "1":
                    hoa_guard.append(str(i))
                else:
                    assert(guard[m] == "0")
                    hoa_guard.append('!' + str(i))
                    
            if len(hoa_guard) == 0:
                hoa_guard.append("t")
            if alive_guard:
                # the automaton starts at time -1
                # this checks that the automaton can advance one time step and
                # start in State 1
                if cur_state == 0:
                    assert(hoa_guard == ["t"])
                    assert(dest_state == 1)
                    
                # # Add transition to dest_state only if it is novel
                # # If it is a proper subset of an existing guard, then
                # # remove this guard from the dictionary
                # hoa_guard = set(hoa_guard)
                # for guard_idx in range(len(guard_dict[dest_state])):
                #     if hoa_guard < guard_dict[dest_state][guard_idx]:
                #         print("Removing redundant transition %s covered by %s" % (str(guard_dict[dest_state][guard_idx]), str(hoa_guard)))
                #         guard_dict[dest_state].pop(guard_idx)
            
                # must_add_guard = True
                # for guard_idx in range(len(guard_dict[dest_state])):
                #     if guard_dict[dest_state][guard_idx] < hoa_guard:
                #         print("Ignoring redundant transition %s covered by %s" % (str(hoa_guard), str(guard_dict[dest_state][guard_idx])))
                #         must_add_guard = False
                #         break
                # if must_add_guard:
                #     guard_dict[dest_state].append(hoa_guard)
                    
                
                hoa += "\n[%s] %s" % ('&'.join(hoa_guard), dest_state)
                # hoa += "\n[%s] %s" % (guard, dest_state)
                if dest_state in accepting_states:
                    hoa += " {0}"
            
    hoa += "\n--END--\n"
    return hoa


def SA2HOA(mona_output):
    # print(mona_output)
    if "Formula is unsatisfiable" in mona_output:
        hoa = UNSAT_HOA
    else:
        hoa = parse_mona(mona_output)
    
    return hoa
    
def Mona2HOA(mona_filename):
    import subprocess
    cmd = "%s %s" %  (MONA,mona_filename)
    # print(cmd)
    p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    mona_output, err = p.communicate()
    mona_output = str(mona_output, 'utf-8').strip()
    # print(mona_output)
    hoa = SA2HOA(mona_output)
    return hoa
    
def ltl2hoa(fml):
    
    # Use the TemporaryFile "formula.mona"
    tmp = tempfile.NamedTemporaryFile(delete=True)
    LTLf2Mona(fml,tmp.name)
    hoa = Mona2HOA(tmp.name)

    return hoa    
    
if __name__ == "__main__":
    import sys
    args = sys.argv[1:]
    
    fml = args[0]
    hoa = ltl2hoa(fml)
    print(hoa)
